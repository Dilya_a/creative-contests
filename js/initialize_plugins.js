//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/




		var sticky 		= $(".sticky"),
			tabs 		= $('.tabs'),
			matchheight = $(".matchheight"),
		    styler 		= $(".styler"),
		    owl 		= $(".owl-carousel"),
		    flex 		= $(".flexslider"),
		    swiper 		= $(".swiper-container"),
		    target 		= $(".target"),
		    dragg 		= $(".dragg"),
		    // draggTwo 	= $('#draggable-wrapper'),
		    // resizable 	= $('#resizable-wrapper'),
		    // elem 		= $('#elem-wrapper');
		    royalslider = $(".royalslider"),
			wow 		= $(".wow"),
			popup 		= $("[data-popup]"),
			superfish 	= $(".superfish"),
			windowW 	= $(window).width(),
			windowH 	= $(window).height();


			if(sticky.length){
					include("plugins/sticky.js");
					include("plugins/jquery.smoothscroll.js");
			}
			if(matchheight.length){
					include("plugins/jquery.matchHeight-min.js");
			}
			if(styler.length){
					include("plugins/formstyler/formstyler.js");
			}
			if(royalslider.length){
					include("plugins/royalslider/jquery.royalslider.min.js");
			}
			if(dragg.length || $(".work_img_box_inner") ){
					include("plugins/jquery-ui/jquery-ui.js");
			}
			if(target.length || $(".work_img_box_inner") ){
					include("plugins/jquery-ui-rotatable/jquery.ui.rotatable.js");
			}
			// if(draggTwo.length || resizable.length || elem.length){
					// include("plugins/draggable-img/jquery.ui.core.js");
					// include("plugins/draggable-img/jquery.ui.widget.js");		
					// include("plugins/draggable-img/jquery.ui.mouse.js");
					// include("plugins/draggable-img/jquery.ui.draggable.js");
					// include("plugins/draggable-img/jquery.ui.resizable.js");
					// include("plugins/draggable-img/jquery.ui.rotatable.js");		
			// }
			if(swiper.length){
					include("plugins/swiper.jquery.js");
			}
			if(wow.length){
					include("plugins/wow.min.js");
			}
			if(popup.length){
					include('plugins/arcticmodal/jquery.arcticmodal.js');
			}
			if(flex.length){
					include('plugins/flexslider/jquery.flexslider.js');
			}
			if(owl.length){
					include('plugins/owl-carousel/owl.carousel.js');
			}
			if(superfish.length){
					include('plugins/superfish/superfish.js');
					// include('plugins/superfish/hoverIntent.js');
			}

					include("plugins/modernizr.js");



			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

		


		$(document).ready(function(){




			/* ------------------------------------------------
			STICKY START
			------------------------------------------------ */

					if (sticky.length){
						$(sticky).sticky({
					        topspacing: 0,
					        styler: 'is-sticky',
					        animduration: 0,
					        unlockwidth: false,
					        screenlimit: false,
					        sticktype: 'alonemenu'
						});
					};

			/* ------------------------------------------------
			STICKY END
			------------------------------------------------ */




			/* ------------------------------------------------
			FORMSTYLER START
			------------------------------------------------ */

					if (styler.length){
						styler.styler({
							// selectSmartPositioning: true
						});
					}

			/* ------------------------------------------------
			FORMSTYLER END
			------------------------------------------------ */




			/* ------------------------------------------------
			FLEXSLIDER START
			------------------------------------------------ */

					if(flex.length){
						flex.flexslider({
						    animation: "slide",
						    controlNav: true,
							animationLoop: false,
							slideshow: false
						});
					}

			/* ------------------------------------------------
			FLEXSLIDER END
			------------------------------------------------ */




			/* ------------------------------------------------
			ROYALSLIDER START
			------------------------------------------------ */

					if(royalslider.length){
						royalslider.royalSlider({
						    fullscreen: false,
						    controlNavigation: 'bullets',
						    autoScaleSlider: true, 
						    autoScaleSliderWidth: 1872, 
						    autoScaleSliderHeight: 650,
						    loop: true,
						    imageScaleMode: 'none',
						    imageAlignCenter: false,
						    navigateByClick: true,
						    numImagesToPreload:2,
						    arrowsNav:true,
						    arrowsNavAutoHide: false,
						    arrowsNavHideOnTouch: true,
						    keyboardNavEnabled: true,
						    fadeinLoadedSlide: true,
						    sliderDrag:false,
						    globalCaption: false,
						    globalCaptionInside: false,
						    imgWidth: 1872,
						    transitionType:'move',
						    autoPlay: {
						      enabled: true,
						      pauseOnHover: false
						    },
						    block: {
						      delay: 400
						    },
						    bullets: {
						      controlsInside : true
						    },
						    thumbs: {
						      appendSpan: false,
						      firstMargin: true,
						      paddingBottom: 0,
						      fitInViewport:false,
						      spacing: 5
						    }
						});
					}

			/* ------------------------------------------------
			ROYALSLIDER END
			------------------------------------------------ */




			/* ------------------------------------------------
			OWL START
			------------------------------------------------ */

					if(owl.length){
						owl.owlCarousel({
							singleItem : true,
							items : 1,
							// loop: true,
							smartSpeed:1000,
							nav: true
							// autoHeight:true
						});
					}

					// if(owl.length){
					//     owl.each(function(){
					//     	var $this = $(this),
					//       		items = $this.data('items');

					//     	$this.owlCarousel({
					//     		singleItem : true,
					// 			items : 1,
					// 			// loop: true,
					// 			smartSpeed:1000,
					// 			// autoHeight:true,
					//     		dots:false,
					//     		nav: true,
					//             navText: [ '', '' ],
					//             // margin: 30,
					//             responsive : items
					//     	});
					//     });
					// }
					// <div class="owl-carousel" data-items='{  "0":{"items":1},   "480":{"items":2},   "991":{"items":3}  }'></div>

			/* ------------------------------------------------
			OWL END
			------------------------------------------------ */




			/* ------------------------------------------------
			TABS START
			------------------------------------------------ */

					if(tabs.length){
						tabs.easyResponsiveTabs();
					}

			/* ------------------------------------------------
			TABS END
			------------------------------------------------ */




			/* ------------------------------------------------
			ANIMATE block START
			------------------------------------------------ */

					if(wow.length){
				        if($("html").hasClass("md_no-touch")){
							new WOW().init();	
						}
						else if($("html").hasClass("md_touch")){
							$("body").find(".wow").css("visibility","visible");
						}

					}

			/* ------------------------------------------------
			ANIMATE block END
			------------------------------------------------ */




			/* ------------------------------------------------
			POPUP START
			------------------------------------------------ */

					if(popup.length){
						popup.on('click',function(){
						    var modal = $(this).data("popup");
						    $(modal).arcticmodal();
						});
					};

			/* ------------------------------------------------
			POPUP END
			------------------------------------------------ */




			/* ------------------------------------------------
			SUPERFISH START
			------------------------------------------------ */

					if(superfish.length){
						superfish.superfish();
					}

			/* ------------------------------------------------
			SUPERFISH END
			------------------------------------------------ */

			/* ------------------------------------------------
			SWIPER START
			------------------------------------------------ */

					if(swiper.length){
						var mySwiper = new Swiper(swiper, {
							scrollbar: '.swiper-scrollbar',
							nextButton: '.swiper-button-next',
        					prevButton: '.swiper-button-prev',
					        scrollbarHide: false,
					        spaceBetween: 10,
							slidesPerView: 6,
					        centeredSlides: false,
					        grabCursor: true
						});

						$(".swiper-button-next").on("click", function(){
							mySwiper.slideNext();
						})
						$(".swiper-button-prev").on("click", function(){
							mySwiper.slidePrev();
						})
					}

			/* ------------------------------------------------
			SWIPER END
			------------------------------------------------ */

			/* ------------------------------------------------
			dragg_box START
			------------------------------------------------ */

					// if(draggTwo.length || resizable.length || elem.length){
					// 	elem.resizable({
					// 		aspectRatio: true,
					// 		handles:     'ne, nw, se, sw',
					// 	});
						
					// 	draggTwo.draggable();
						
					// 	elem.parent().rotatable();
					// }

			/* ------------------------------------------------
			dragg_box END
			------------------------------------------------ */

			/* ------------------------------------------------
			dragg START
			------------------------------------------------ */

					if(dragg.find('img').length){
						dragg.draggable();
					}

			/* ------------------------------------------------
			dragg END
			------------------------------------------------ */

			/* ------------------------------------------------
			dragg START
			------------------------------------------------ */
			var params = {
	            // Callback fired on rotation start.
	            start: function(event, ui) {
	            },
	            // Callback fired during rotation.
	            rotate: function(event, ui) {
	            },
	            // Callback fired on rotation end.
	            stop: function(event, ui) {
	            },
	            // Set the rotation center at (25%, 75%).
	            rotationCenterX: 25.0, 
	            rotationCenterY: 75.0
	        };

					if(target.length){
				        $(target).resizable().rotatable(params);
					}

			/* ------------------------------------------------
			dragg END
			------------------------------------------------ */

			var counter = 0;


			$(".swiper-slide").on("click ontouchstart", function(){
				$(this).find('span').clone().addClass('target dragg item'+counter+'').prependTo(".work_img_box_inner");
				$(".target").resizable().rotatable(params);
				$(".dragg").draggable();
				$(".work_img_box_inner").find('span.item'+counter+'').append('<span class="dragg_icon"></span>');

				counter += 1;
			});



		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
