$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ


$(document).ready(function(){


		var stepsBox = $("#collazh_box"),
			collazhContainer = $("#collazh_container");

		function swicher(step, image, value){
			
			stepsBox.find(">div").removeClass('active');
			stepsBox.find(">div").eq(step).addClass('active');

			if (step == 1){
				if (image){
					collazhContainer.css('background-image', 'url('+image+')');
				}

				$("#img-out").find('canvas').remove();

			}
			else if (step == 2){

			}
		}

		$("#publicBtn").on('click', function(event){
			event.preventDefault();

			html2canvas(document.querySelector('#collazh_container'), {
	            onrendered: function(canvas) {
	            	var imageData  = canvas.getContext("2d").getImageData(0, 0, canvas.width, canvas.height);

	            	// console.log(imageData);

	            	// var resultCanvas = document.createElement("canvas");

	            	// resultCanvas.width = canvas.width;
	            	// resultCanvas.height = canvas.height;

	            	// resultCanvas.getContext("2d").putImageData(imageData, 0, 0, 0, 0, canvas.width, canvas.height);

	                // document.body.appendChild(resultCanvas);

	                // Convert and download as image 
	                // Canvas2Image.saveAsPNG(canvas); 
	                $("#img-out").append(canvas);
	                // Clean up 
	                // document.body.removeChild(canvas);
	            }
	        });
		})


		$('.swichBtn').on('click', function(event){
			event.preventDefault();

			var current = $(this),
				step    = current.data('step'),
				image   = current.attr('href');

			if (step == 2){
				setTimeout(swicher, 100, step, image);
			}
			else{
				swicher(step, image)
			}

		})




		/* ------------------------------------------------
		ANCHOR START
		------------------------------------------------ */
				 
			    function goUp(){
					var windowHeight = $(window).height(),
						windowScroll = $(window).scrollTop();

					if(windowScroll>windowHeight/2){
						$('.arrow_up').addClass('active');
					}

					else{
						$('.arrow_up').removeClass('active');
					}

			    }

			    goUp();
				$(window).on('scroll',goUp);

				$('.arrow_up').on('click ontouchstart',function () {

					if($.browser.safari){
						$('body').animate( { scrollTop: 0 }, 1100 );
					}
					else{
						$('html,body').animate( { scrollTop: 0}, 1100 );
					}
					return false;
					
				});

		/* ------------------------------------------------
		ANCHOR END
		------------------------------------------------ */



		/* ------------------------------------------------
		RESPONSIVE MENU START
		------------------------------------------------ */

				// $(".resp_btn, .close_resp_menu").on("click ontouchstart", function(){
				// 	$("body").toggleClass("show_menu")
				// });

				// $(document).on("click ontouchstart", function(event) {
			 //      if ($(event.target).closest("nav,.resp_btn").length) return;
			 //      $("body").removeClass("show_menu");
			 //      if(windowW <= 991){
			 //      	$(".menu_item").removeClass("active").find(".dropdown-menu").css("display","none");
			 //      }
			 //      event.stopPropagation();
			 //    });

				// проверка на наличие элемента и вставка хтмл кода
			 	//  if($(window).width() <= 767){
				// 	if ($(".menu_item").length){
				//         $(".menu_item").has('.dropdown-menu').append('<span class="touch-button"><i class="fa fa-angle-down"></i></span>');
				//     }
				// }
				// проверка на наличие элемента и вставка хтмл кода


				$('.menu_link').on('click ontouchstart',function(event){
					if($("html").hasClass("md_no-touch"))return;

			        var windowWidth = $(window).width(),
			            $parent = $(this).parent('.menu_item');
			        if(windowWidth > 991){
			          // if($("html").hasClass("md_touch")){
			            if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

			              event.preventDefault();

			              $parent.toggleClass('active')
			               .siblings()
			               .find('.menu_link')
			               .removeClass('active');
			            }
			          // }  
			        }
			        
			        else{
			            
			          if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

			            event.preventDefault();

			            $parent.toggleClass('active')
			             .siblings()
			             .removeClass('active');
			            $parent.find(".dropdown-menu")
			             .slideToggle()
			             .parents('.menu_item')
			             .siblings()
			             .find(".dropdown-menu")
			             .slideUp();
			          }
			        }

			    });



					// $('.categories_lk , .sale_primary_nav__list > li > a, .sale_primary_nav__list_link').on('click ontouchstart',function(event){
			  //       	if($("html").hasClass("md_no-touch"))return;  

			  //           var windowWidth = $(window).width(),
			  //               $parent = $(this).parent('.menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it');
			  //           if(windowWidth > 767){
			  //             // if($("html").hasClass("md_touch")){
			  //               if((!$parent.hasClass('active')) && $parent.find('[class*="subcategories_show"]').length){

			  //                 event.preventDefault();

			  //                 $parent.toggleClass('active')
			  //                  .siblings()
			  //                  .find('.categories_lk , .sale_primary_nav__list > li > a, .sale_primary_nav__list_link')
			  //                  .removeClass('active');
			  //               }
			  //             // }  
			  //           }
			            
			  //           else{
			                
			  //             if((!$parent.hasClass('active')) && $parent.find('[class*="subcategories_show"]').length){

			  //               event.preventDefault();

			  //               $parent.toggleClass('active')
			  //                .siblings()
			  //                .removeClass('active');
			  //               $parent.find("[class*='subcategories_show']")
			  //                .slideToggle()
			  //                .parents('.menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it')
			  //                .siblings()
			  //                .find("[class*='subcategories_show']")
			  //                .slideUp();
			  //             }
			  //           }

			  //       });


			  //       $(document).on("click ontouchstart", function(event) {
			  //         if ($(event.target).closest(".categories_lk , .sale_primary_nav__list > li > a, .sale_primary_nav__list_link").length) return;
			  //         $(".menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it").removeClass("active");
			  //         $(".menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it").find("[class*='subcategories_show']").css("display","none");
			  //         event.stopPropagation();
			  //       });

		/* ------------------------------------------------
		RESPONSIVE MENU END
		------------------------------------------------ */



/*---------------------------- START: image changing -------------------------*/
	/*копирует src of img и подставляет его в фон этого же блока(можно изменять позицию бг)*/

   //   if($(".pizza_content_link_img").length){
   //      $(".pizza_content_link_img > img").each(function(){
   //        var current = $(this),
   //            parent = current.parent(".pizza_content_link_img"),
   //            linkAttr = current.attr("src");

   //          parent.css({'background-image' : "url(" + linkAttr + ")"});
   //          console.log(1);
   //      });
   //    }
/*---------------------------- END: image changing -------------------------*/



		
});